﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";
    var appdata = Windows.Storage.ApplicationData;

    WinJS.UI.Pages.define("/pages/about/about.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            // TODO: Initialize the page here.
            var theme = appdata.current.roamingSettings.values["theme"];
            theme = !theme ? "light" : theme; // light if value is undefined
            document.getElementById("mainStyle").src = "//Microsoft.WinJS.2.0/css/ui-ui-" + theme + ".css";
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
