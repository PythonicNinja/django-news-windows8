﻿// For an introduction to the Page Control template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232511
(function () {
    "use strict";
    var appdata = Windows.Storage.ApplicationData;

    WinJS.UI.Pages.define("/pages/preferences/preferences.html", {
        // This function is called whenever a user navigates to this page. It
        // populates the page elements with the app's data.
        ready: function (element, options) {
            var themeSelect = document.getElementById("theme");

            var theme = appdata.current.roamingSettings.values["theme"];
            theme = !theme ? "light" : theme; // light if value doesn’t exist
            document.getElementById("mainStyle").src = "//Microsoft.WinJS.2.0/css/ui-ui-" + theme + ".css";

            themeSelect.value = theme;

            themeSelect.addEventListener("change", function (e) {
                appdata.current.roamingSettings.values["theme"] = e.target.value;                
                document.getElementById("mainStyle").src = "//Microsoft.WinJS.2.0/css/ui-ui-" + appdata.current.roamingSettings.values["theme"] + ".css";
            });
        },

        unload: function () {
            // TODO: Respond to navigations away from this page.
        },

        updateLayout: function (element) {
            /// <param name="element" domElement="true" />

            // TODO: Respond to changes in layout.
        }
    });
})();
