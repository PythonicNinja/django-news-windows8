﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509
(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    var appdata = Windows.Storage.ApplicationData;
    

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {

            
            var theme = appdata.current.roamingSettings.values["theme"];
            theme = !theme ? "light" : theme; // light if value is undefined    
            document.getElementById("mainStyle").src = "//Microsoft.WinJS.2.0/css/ui-ui-" + theme + ".css";

            
            $('body').on('click', '.link', function (e) {
                e.preventDefault();
                var $this = $(this);
                var $href = $this.attr('href');
                $('.close').fadeIn("slow");
                $('#externalPage').attr('src', $href).fadeIn("slow");
            });

            $('.close').on('click', function () {
                $('.close').fadeOut("slow");
                $('#externalPage').fadeOut("slow");
            });

            
            


            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }


            app.onsettings = function (e) {
                e.detail.applicationcommands = {
                    // Add an About command
                    "about": {
                        href: "/pages/about/about.html",
                        title: "About"
                    },
                    "preferences": {
                        href: "/pages/preferences/preferences.html",
                        title: "Preferences"
                    }
                }

                WinJS.UI.SettingsFlyout.populateSettings(e);
            };

            var articlesList = new WinJS.Binding.List();
            var publicMembers = { ItemList: articlesList };
            WinJS.Namespace.define("DjangoData", publicMembers);


            var downloadFeed = function(url) {
                WinJS.xhr({ url: url[0] }).then(function (rss) {
                    var items = rss.responseXML.querySelectorAll("item");

                    for (var n = 0; n < items.length; n++) {
                        var article = {};
                        article.category = url[1];
                        article.title = items[n].querySelector("title").textContent;
                        article.description = items[n].querySelector("description").textContent;
                        if(items[n].querySelector("link")){
                            article.link = items[n].querySelector("link").textContent;
                        }
                        articlesList.push(article);
                    }                    
                });
            }

            var urls = [
                ["https://www.djangoproject.com/rss/community/blogs/", "Blogs"],
                ["https://www.djangoproject.com/rss/community/jobs/", "Jobs"],
                ["https://www.djangoproject.com/rss/community/q-and-a/", "Q&A"],
                ["https://www.djangoproject.com/rss/community/links/", "Links"],
                ["https://www.djangoproject.com/rss/community/packages/", "Packages"],
            ]

            var feedsDownload = function () {
                for (var i = 0; i < urls.length; i++) {
                    downloadFeed(urls[i]);
                }
            };


            // App bar event listeners
            document.getElementById('refresh').addEventListener("click", function (e) {
                feedsDownload();
            });
            document.getElementById('settings').addEventListener("click", function (e) {
                WinJS.UI.SettingsFlyout.show();
            });
            document.getElementById('about').addEventListener("click", function (e) {
                WinJS.UI.SettingsFlyout.show();
            });


            var searchPane = Windows.ApplicationModel.Search.SearchPane.getForCurrentView();
            searchPane.onquerysubmitted = function (args) {
                var queryText = args.queryText;

                feedsDownload();
                var results = [];
                for (var i = 0; i < articlesList.length; i++) {
                    if (articlesList.getItem(i).data.title.indexOf(args.queryText) > -1) {
                        results.push(articlesList.getItem(i).data);
                    }
                }
                var articlesBindingList = new WinJS.Binding.List(results);
                assignToListView(articlesBindingList);
            }


            var assignToListView = function (articlesBindingList) {

                var articlesGenresList = articlesBindingList.createGrouped(
                        function(item) {return item.category},
                        function(item) {return item.category}
                    );

                var articlelist = document.getElementById('articlelist').winControl;
                articlelist.itemDataSource = articlesGenresList.dataSource;
                articlelist.groupDataSource = articlesGenresList.groups.dataSource;

            }


            var dtm = Windows.ApplicationModel.DataTransfer.DataTransferManager.getForCurrentView();

            dtm.ondatarequested = function (args) {

                var deferral = args.request.getDeferral();
                var text = "";
                articlelist.winControl.selection.getItems().done(
                    function (items) {
                        if (items.length > 0) {
                            for (var i = 0; i < items.length; i++) {
                                text += items[i].data.category + "   -   " + items[i].data.title + "   -   " + items[i].data.link + "\n";
                            }
                            if(items.length==1)
                                args.request.data.properties.title = items[0].data.title;
                            else
                                args.request.data.properties.title = "Django news shared";
                        } 
                    }
               );
                
                args.request.data.setText(text);

                deferral.complete();

            }




            args.setPromise(WinJS.UI.processAll().then(feedsDownload).then(assignToListView(articlesList)));
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. You might use the
        // WinJS.Application.sessionState object, which is automatically
        // saved and restored across suspension. If you need to complete an
        // asynchronous operation before your application is suspended, call
        // args.setPromise().

    };

    app.start();
})();
